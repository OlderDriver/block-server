package com.ugchain.wallet.server.block.service;

import java.util.concurrent.CountDownLatch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.methods.response.Transaction;
import org.web3j.protocol.http.HttpService;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ugchain.wallet.server.block.config.BlockConfig;

import rx.Observable;
import rx.Subscription;

/**
 * @ClassName: SavePendingTransaction
 * @Description: TODO(保存pending状态交易到redis)
 * @author OldDriver
 * @date 2017年6月12日
 * 
 */

@Component
@Scope("prototype")
public class SavePendingTransaction implements Runnable {

    private static final Logger LOGGER = LoggerFactory.getLogger(SavePendingTransaction.class);

    private static final ObjectMapper MAPPER = new ObjectMapper();

    private Web3j web3j;

    @Autowired
    private BlockConfig blockConfig;

    @Autowired
    private BlockService blockService;

    @Autowired
    private PendingTransactionService pendingTransactionService;

    // 初始化web3j对象，创建连接
    @Override
    public void run() {
	// blockService.init();
	web3j = Web3j.build(new HttpService(blockConfig.getGethHttpURL()));

	Observable<Transaction> pendingTransactionObservable = web3j.pendingTransactionObservable();

	CountDownLatch transactionLatch = new CountDownLatch(1);

	CountDownLatch completedLatch = new CountDownLatch(1);

	Subscription subscription = pendingTransactionObservable.onErrorResumeNext(web3j.pendingTransactionObservable())
		.subscribe(result -> {
		    System.out.println(result.getFrom() + "------" + result.getTo() + "------" + result.getHash());

		    pendingTransactionService.saveTransaction(result);
		    
		    
		}, throwable -> {
		    LOGGER.error("listen pendingTransaction error:", throwable);
		}, () -> completedLatch.countDown());

	try {
	    transactionLatch.await();
	} catch (InterruptedException e) {
	    LOGGER.error("subscription wait interrupted!!!!!!");
	}

    }

}
