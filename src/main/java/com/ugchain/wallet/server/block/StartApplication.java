package com.ugchain.wallet.server.block;


import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

import com.ugchain.wallet.server.block.dao.RedisDao;
import com.ugchain.wallet.server.block.dao.RedisDaoImpl;
import com.ugchain.wallet.server.block.service.BlockService;
import com.ugchain.wallet.server.block.service.PendingTransactionServiceImpl;
import com.ugchain.wallet.server.block.service.SavePendingTransaction;



@Configuration
@ComponentScan(basePackages={"com.ugchain.wallet.server.block"})
//@SpringBootApplication
@ImportResource({ "classpath:application-context.xml" })
@EnableAutoConfiguration(exclude = { DataSourceAutoConfiguration.class,
	DataSourceTransactionManagerAutoConfiguration.class, HibernateJpaAutoConfiguration.class })
public class StartApplication extends SpringBootServletInitializer {
    private static final Logger logger = LoggerFactory.getLogger(StartApplication.class);

    public static void main(String[] args) {
	System.setProperty("java.net.preferIPv4Stack", "true");
	ApplicationContext context = SpringApplication.run(StartApplication.class, args);
	context.getBean(RedisDaoImpl.class).initMasterjedisPool();
	SavePendingTransaction savePendingTransaction = context.getBean(SavePendingTransaction.class);
	new Thread(savePendingTransaction).start();
	
	try {
	    context.getBean(BlockService.class).queryBlock();
	} catch (BeansException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	} catch (IOException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	} catch (InterruptedException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	
//	context.getBean(UGWeb3jServiceImpl.class).queryBalance("0xc6e76208d475844d2ac31744bd2a6420031fd066");
/*	try {
	    context.getBean(BlockService.class).queryBlock();
	} catch (BeansException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	} catch (IOException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	} catch (InterruptedException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}*/
    }

}
