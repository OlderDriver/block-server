package com.ugchain.wallet.server.block.service;

import com.ugchain.wallet.server.block.config.BlockConfig;
import com.ugchain.wallet.server.block.dao.RedisDao;
import com.ugchain.wallet.server.block.utils.InputUtil;
import com.ugchain.wallet.server.block.vo.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.web3j.protocol.core.methods.response.Transaction;
import java.util.*;

/**
 * @ClassName: PendingTransactionServiceImpl
 * @Description: TODO(同步pending状态交易服务实现)
 * @author OldDriver
 * @date 2017年6月12日
 * 
 */

@Service
public class PendingTransactionServiceImpl implements PendingTransactionService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PendingTransactionServiceImpl.class);

    @Autowired
    private BlockConfig blockConfig;

    @Autowired
    private RedisDao redisDao;

    @Autowired
    private InputUtil inputUtil;
    // public void init() {
    //
    // writeJedis = new JedisPool(blockConfig.getRedisUrl(),
    // Integer.parseInt(blockConfig.getRedisPort()));
    //// jedis.select(8);
    // }

    public void saveTransaction(Transaction result) {

	// 数据处理
	String input = result.getInput();

	int coinType = inputUtil.judgeInput(input);

	String hash = null;
	String from = null;
	String to = null;
	
	switch (coinType) {
	case Constants.coin_type_ETH:
	    hash = result.getHash();
	    from = result.getFrom();
	    to = result.getTo();

	    if (from != null && from != "") {
		redisDao.insertPendingTr(from, hash);
	    }
	    if (to != null && to != "") {
		redisDao.insertPendingTr(to, hash);
	    }
	    break;
	case Constants.coin_type_UGT:
	    
	    Map<String, Object> map = inputUtil.resolveInput(input);
	    hash = result.getHash();
	    from = result.getFrom();

	    if (from != null && from != "") {
		redisDao.insertPendingTr(from, hash);
	    }
	default:
	    
	    break;
	}

	// System.out.println(from + "--------------" +
	// Thread.currentThread().getName());
    }
}